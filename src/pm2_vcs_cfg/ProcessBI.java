package pm2_vcs_cfg;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class ProcessBI
{
  public static BufferedImage resizeImage(BufferedImage originalImage,
                 int imageWidth,int imageHeight)
  {
    if(originalImage==null) return null;
    int iH=originalImage.getHeight();
    int iW=originalImage.getWidth();
    if((iH==imageHeight) && (iW==imageWidth)) return originalImage;
    float fH=(float)iH/imageHeight;
    float fW=(float)iW/imageWidth;
    if(fH>fW) fW=fH;
    else fH=fW;
    iH=(int)(originalImage.getHeight()/fH);
    iW=(int)(originalImage.getWidth()/fW);
    BufferedImage resizedImage = new BufferedImage(iW,iH,originalImage.getType());
    Graphics2D g=resizedImage.createGraphics();
    g.drawImage(originalImage,0,0,iW,iH,null);
    g.dispose();
    return resizedImage;
  }

  public static void mirrorImage(BufferedImage originalImage)
  {
    if(originalImage==null) return;
    int iH=originalImage.getHeight();
    int iW=originalImage.getWidth()/2;
    int iRGB;
    for(int y=0;y<iH;y++)
    {
      for(int lx=0,rx=iW*2-1;lx<iW;lx++,rx--)
      {
        iRGB=originalImage.getRGB(rx,y);
        originalImage.setRGB(rx,y,originalImage.getRGB(lx,y));
        originalImage.setRGB(lx,y,iRGB);
      }
    }
  }
}
