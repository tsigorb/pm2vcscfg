package pm2_vcs_cfg;

public class XchgPckt
{
  Integer iOp;  // for Out queue: 0-none, 1-connect, 2-disconnect, 3-write
                // for In queue: 4-read, 10-answ.unknown,11-answ.connect,12-answ.disconnect,13-answ.write
  String strData;
  Integer iData;

  XchgPckt()
  { iOp=0; iData=0; }
}
