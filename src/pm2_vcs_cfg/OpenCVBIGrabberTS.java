package pm2_vcs_cfg;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class OpenCVBIGrabberTS
{
    private int deviceNumber = 0;
    private int imageWidth=0;
    private int imageHeight=0;
    private boolean m_bRotate=false;
    private boolean m_bMirror=false;
    private int VCamWidth=0,VCamHeight=0,VCamSize=0;
    private VideoTS VI=null;

  public OpenCVBIGrabberTS(int deviceNumber)
  {
    this.deviceNumber = deviceNumber;
  }

  @Override
  protected void finalize() throws Throwable
  { super.finalize(); stop(); }

  public void setWidth(int iWidth)
  { imageWidth=iWidth; }

  public void setHeight(int iHeight)
  { imageHeight=iHeight; }

  public Dimension getVCamDimension()
  {
    Dimension dm=new Dimension();
    dm.width=VCamWidth; dm.height=VCamHeight;
    return dm;
  }

  public void setRotate(boolean bRotate)
  { m_bRotate=bRotate; }

  public void setMirror(boolean bMirror)
  { m_bMirror=bMirror; }

  public boolean start(int iW,int iH)
  {
    imageWidth=iW; imageHeight=iH;
    VI=new VideoTS();
    if(VI!=null)
    {
      VI.setupDevice(deviceNumber);
      VCamWidth=VI.getWidth(deviceNumber);
      VCamHeight=VI.getHeight(deviceNumber);
      VCamSize=VI.getSize(deviceNumber);
      if((VCamWidth>0) && (VCamHeight>0) && (VCamSize>0))
      { return true; }
    }
    return false;
  }

  public void showSettings()
  {
    if(VI!=null) VI.showSettingsWindow(deviceNumber);
  }

  public void stop()
  {
    if(VI!=null)
    {
      VI.stopDevice(deviceNumber);
      VI.close();
      VI=null;
    }
  }

  public BufferedImage grab()
  {
      if(VI.isFrameNew(deviceNumber))
      {
        BufferedImage img=new BufferedImage(VCamWidth,VCamHeight,BufferedImage.TYPE_3BYTE_BGR);
//        if(img!=null)
        {
          byte[] targetPixels=((DataBufferByte)img.getRaster().getDataBuffer()).getData();
          if(VI.getPixels(deviceNumber,targetPixels,false,!m_bRotate))
          {
            img=ProcessBI.resizeImage(img,imageWidth,imageHeight);
            if(m_bMirror) ProcessBI.mirrorImage(img);
            return img;
          }
        }
      }
    return null;
  }

/*  public Frame grabFrame()
  {
    if(VI.isFrameNew(deviceNumber))
    {
      if(VI.getPixels(deviceNumber, VCamBuf,false,!m_bRotate))
      {
        BufferedImage img=new BufferedImage(VCamWidth,VCamHeight,BufferedImage.TYPE_3BYTE_BGR);
        byte[] targetPixels=((DataBufferByte)img.getRaster().getDataBuffer()).getData();
        System.arraycopy(VCamBuf,0,targetPixels,0,VCamBuf.length);
        if(m_bMirror) img=ProcessBI.mirrorImage(img);
        return Java2DFrameUtils.toFrame(img);
      }
    }
    return null;
  }*/
}

/*  BufferedImage Mat2BufferedImage(Mat m)
  {
    int type;

    if(m.channels()>1) type = BufferedImage.TYPE_3BYTE_BGR;
    else type = BufferedImage.TYPE_BYTE_GRAY;
    int bufferSize = m.channels() * m.cols() * m.rows();
    byte[] b = new byte[bufferSize];
    m.get(0, 0, b); // get all the pixels
    BufferedImage img = new BufferedImage(m.cols(), m.rows(), type);
    byte[] targetPixels=((DataBufferByte)img.getRaster().getDataBuffer()).getData();
    System.arraycopy(b,0,targetPixels,0,b.length);
    return ProcessBI.resizeImage(img,imageWidth,imageHeight);
  }*/
