package pm2_vcs_cfg;

import java.io.File;
import java.lang.reflect.Field;

public class LoadNativeLib
{
  private static void setLibraryPath(String s)
  {
    try
    {
      Field fieldPath = ClassLoader.class.getDeclaredField("usr_paths");
      fieldPath.setAccessible(true);
      String[] paths = (String[])fieldPath.get(null);
      for(int i = 0; i < paths.length; i++)
      {
        if(s.equals(paths[i])) return;
      }
      String[] tmp = new String[paths.length+1];
      System.arraycopy(paths,0,tmp,0,paths.length);
      tmp[paths.length] = s;
      fieldPath.set(null,tmp);
      System.setProperty("java.library.path",
                         System.getProperty("java.library.path")+File.pathSeparator+s);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      throw new RuntimeException(ex);
    }
  }
  
  public static void LoaderNativeLibs(String strPath)
  {
    if((strPath==null) || strPath.isEmpty()) strPath=".\\Lib";
    setLibraryPath(strPath);
    //System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
  }

  public LoadNativeLib()
  { }
}
