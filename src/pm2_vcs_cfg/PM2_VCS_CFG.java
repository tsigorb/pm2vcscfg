package pm2_vcs_cfg;

import com.fazecast.jSerialComm.*;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.*;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.text.*;

public class PM2_VCS_CFG extends javax.swing.JFrame
{
  ///////////////////////////////////////////////////////////////////
  class ByteDocument extends PlainDocument
  {
    int m_iMax=255;

    ByteDocument(int iMax)
    { super(); m_iMax=iMax; }

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    {
      super.insertString(offs, revise(str,3-getLength()), a);
      String strInp=getText(0,getLength());
      if(strInp.isEmpty()) return;
      int iInp=Integer.parseInt(strInp);
      if(iInp>255) super.replace(0,3,strInp.substring(0,2),a);
    }

    @Override
    public void replace(int offset, int length, String text,
                        AttributeSet a) throws BadLocationException
    {
      super.replace(offset,length,revise(text,3-getLength()+length),a);
      String strInp=getText(0,getLength());
      if(strInp.isEmpty()) return;
      int iInp=Integer.parseInt(strInp);
      if(iInp>m_iMax) super.replace(0,3,strInp.substring(0,2),a);
    }

    private String revise(String strInsert,int iMax)
    {
      int index = 0;
      while((index<strInsert.length()) && (index<iMax))
      {
        if(Character.isDigit(strInsert.charAt(index))) index++;
        else break;
      }
      return strInsert.substring(0,index);
    }
  }

  class PortDocument extends PlainDocument
  {
    PortDocument()
    { super(); }

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    { super.insertString(offs, revise(str,5-getLength()), a); }

    @Override
    public void replace(int offset, int length, String text,
                        AttributeSet attrs) throws BadLocationException
    { super.replace(offset,length,revise(text,5-getLength()+length),attrs); }

    private String revise(String strInsert,int iMax)
    {
      int index = 0;
      while((index<strInsert.length()) && (index<iMax))
      {
        if(Character.isDigit(strInsert.charAt(index))) index++;
        else break;
      }
      return strInsert.substring(0,index);
    }
  }

  class IpDocument extends PlainDocument
  {
    IpDocument()
    { super(); }

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
    { super.insertString(offs,revise(str,15-getLength()),a); }

    @Override
    public void replace(int offset, int length, String text,
                        AttributeSet attrs) throws BadLocationException
    { super.replace(offset,length,revise(text,15-getLength()+length),attrs); }

    private String revise(String strInsert,int iMax)
    {
      int index = 0;
      while((index<strInsert.length()) && (index<iMax))
      {
        if(accept(strInsert.charAt(index))) index++;
        else break;
      }
      return strInsert.substring(0,index);
    }

    private boolean accept(final char c)
    { return Character.isDigit(c) || (c=='.'); }
  }

  // ����� ����������� ������� � ������ �������
  class CenterCellRenderer extends DefaultTableCellRenderer
  {
    @Override
    public Component getTableCellRendererComponent(JTable table,Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row, int column)
    { // ����� ��������� ���������� ��� ����������
      // ������� �� �������� ������
      JLabel label=(JLabel)super.getTableCellRendererComponent(table,
                                 value,isSelected,hasFocus,row,column);
      label.setHorizontalAlignment(JLabel.CENTER);
      return label;
    }
  }

  ///////////////////////////////////////////////////////////////////
  Pattern IpPattern;
  SerialPort MassRS232[];
  SerialPort VCamIF[];

  int iStateButtonWrite;
  protected static int c_iIP=0x01;
  protected static int c_iPort=0x02;
  protected static int c_iMask=0x04;
  protected static int c_iGate=0x08;
  protected static int c_iCOM=0x10;

  protected static int c_MaxPreset=127; // 0..255

  final static String c_strFName="PM2VCSCFG.ini";
  final static String c_strPATTERN=
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

  private static int m_iWebCamInd = -1;
  OpenCVBIGrabberTS grabber;
  BufferedImage VCamBI=null;
  Timer VCamTimer;
  Timer VCamZoomTimer;
  Timer VCamPTTimer;
  Timer JDlgTCPTimer;

  int m_iFrameNull=0;
  int m_iPan=0;
  int m_iTilt=0;

  CanvasFrameTS cFrame=null;

  LinkedBlockingQueue<XchgPckt> m_bqIn;
  LinkedBlockingQueue<XchgPckt> m_bqOut;

  ///////////////////////////////////////////////////////////////////
  public PM2_VCS_CFG()
  {
    this.grabber=null;
    LoadNativeLib.LoaderNativeLibs(null);

    initComponents();

    try
    { setIconImage(new ImageIcon("./PM2VCSCFG.gif").getImage()); }
    catch(Exception ex)
    { System.out.println("PM2_VCS_CFG.IconImage: " + ex.toString()); }
    setLocationRelativeTo(null); // ������������� ��������� �� ������ ������

    iStateButtonWrite=0;
    IpPattern = Pattern.compile(c_strPATTERN);

    IpInputCfg();
    PortInputCfg();
    MaskInputCfg();
    GateInputCfg();
    MicTableCfg();
    VCamSNTableCfg();
    ReadCfgFromFile();

    RS232Init4BSV();

    VCamTimer=new Timer(20,(java.awt.event.ActionEvent e)->
    {
      Graphics vcg;
      Canvas curCanvas;
      if(cFrame==null) curCanvas=VCamCanvas;
      else curCanvas=cFrame.getCanvas();
      VCamBI = grabber.grab();
      if(VCamBI!=null)
      {
        m_iFrameNull=0;
        vcg=curCanvas.getGraphics();
        int iX=(curCanvas.getWidth()-VCamBI.getWidth())/2;
        int iY=(curCanvas.getHeight()-VCamBI.getHeight())/2;
        vcg.drawImage(VCamBI,iX,iY,null);
        vcg.dispose();
      }
      else
      {
        if(m_iFrameNull<50) m_iFrameNull++;
        else
        {
          String strInfo="WAIT VIDEO...";
          vcg=curCanvas.getGraphics();
          int iStrW=vcg.getFontMetrics().stringWidth(strInfo);
          vcg.drawString(strInfo,
                         (curCanvas.getWidth()-iStrW)/2,curCanvas.getHeight()/2);
          vcg.dispose();
        }
      }
    });
    VCamTimer.setRepeats(true);
    VCamPageInit();
    
    m_bqOut=new LinkedBlockingQueue();
    m_bqIn=new LinkedBlockingQueue();
    JDlgTCPTimer=new Timer(200,(java.awt.event.ActionEvent e)->
    { JDlgTCPTimerEvent(); });
    JDlgTCPTimer.setRepeats(true);
    jButtonWrite.setEnabled(false);
    jComboBoxRS232.setEnabled(false);
  }

  void VCamPageInit()
  {
    int i,n;

    jTF_VCamPreset.setDocument(new ByteDocument(c_MaxPreset));
    jTF_VCamPreset.setText("0");
    VCamIF=SerialPort.getCommPorts();
    for(i=0;i<VCamIF.length;i++)
    {
        jCB_VCamIF.addItem(VCamIF[i].getSystemPortName());
    }
    if(jCB_VCamIF.getItemCount()>0) jCB_VCamIF.setSelectedIndex(0);
    else
    {
      jButtonCallPreset.setEnabled(false);
      jButtonDelPreset.setEnabled(false);
      jButtonSavePreset.setEnabled(false);
    }
    n = VideoTS.listDevices();
    if(n!=0)
    {
      for(i=0;i<n;i++)
      {
        String strName=VideoTS.getDeviceName(i);
        jCB_VCam.addItem(strName);
      }
      m_iWebCamInd=0;
      jCB_VCam.setSelectedIndex(m_iWebCamInd);
      jCB_VCam.addActionListener(new java.awt.event.ActionListener()
      {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
          m_iWebCamInd=jCB_VCam.getSelectedIndex();
          StartGrabber();
        }
      });
      jCB_VCamProto.setSelectedItem("Visca");
      jCB_VCamSpeed.setSelectedItem("9600");
      VCamZoomTimer=new Timer(1500,(java.awt.event.ActionEvent e)->
      { VCamZoom_SendCmd(0); });
      VCamZoomTimer.setRepeats(false);
      VCamPTTimer=new Timer(200,(java.awt.event.ActionEvent e)->
      { VCamPTDrive_SendCmd(m_iPan,m_iTilt); });
      VCamPTTimer.setRepeats(true);
      StartGrabber();
    }
    else
    {
      jButtonCallPreset.setEnabled(false);
      jButtonDelPreset.setEnabled(false);
      jButtonSavePreset.setEnabled(false);
    }
  }

  void VCamSNTableCfg()
  {
    jTableVCamSN.getColumnModel().getColumn(0).setCellRenderer(new CenterCellRenderer());
    jTableVCamSN.getColumnModel().getColumn(1).setCellRenderer(new CenterCellRenderer());

    JComboBox<String> combo; // �������������� ������
    combo=new JComboBox<>(new String[] {"","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15"});
    jTableVCamSN.getColumnModel().getColumn(0).setCellEditor(
                 new DefaultCellEditor(combo)); // �������� ������ � �������������� �������

     int iWidthT=jTableVCamSN.getWidth();
     jTableVCamSN.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
     jTableVCamSN.getColumnModel().getColumn(0).setPreferredWidth(iWidthT/3);
     jTableVCamSN.getColumnModel().getColumn(1).setPreferredWidth(iWidthT-iWidthT/3);
  }

  void MicTableCfg()
  {
    int i;

    // ����� ������ �� ������ ������
    jTableMicVCS.getColumnModel().getColumn(0).setCellRenderer(new CenterCellRenderer());
    jTableMicVCS.getColumnModel().getColumn(1).setCellRenderer(new CenterCellRenderer());
    jTableMicVCS.getColumnModel().getColumn(2).setCellRenderer(new CenterCellRenderer());

    for(i=0;i<jTableMicVCS.getRowCount();i++)
    { jTableMicVCS.setValueAt(i+1,i,0); }

    JComboBox<String> combo; // �������������� ������
    combo=new JComboBox<>(new String[] {"","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15"});
    jTableMicVCS.getColumnModel().getColumn(1).setCellEditor(
                 new DefaultCellEditor(combo)); // �������� ������ � �������������� �������

    JTextField byteField=new JTextField();
    byteField.setDocument(new ByteDocument(c_MaxPreset));
    jTableMicVCS.getColumnModel().getColumn(2).setCellEditor(
                 new DefaultCellEditor(byteField));
  }

  void ExitIfNeed()
  {
    Object[] options = { "��", "���!" };
    int n = JOptionPane.showOptionDialog(this,
                             "����� �� ��������� PM2_VCS_CFG?",
                             "�������������",
                             JOptionPane.YES_NO_OPTION,
                             JOptionPane.QUESTION_MESSAGE,
                             null, options,options[0]);
    if(n==0)
    {
      setVisible(false);
      System.exit(0);
    }
  }

  void RS232Init4BSV()
  {
    int i;

    MassRS232=SerialPort.getCommPorts();
    for(i=0;i<MassRS232.length;i++)
    {
      if(MassRS232[i].getPortDescription().indexOf("FT230X Basic UART")>-1)
      {
        jComboBoxRS232.addItem(MassRS232[i].getSystemPortName());
      }
    }
    if(jComboBoxRS232.getItemCount()>0)
    {
      jComboBoxRS232.setSelectedIndex(0);
      iStateButtonWrite|=c_iCOM;
    }
    else jButtonWrite.setText("��������� � ����");
  }

  void IpInputCfg()
  {
    jTF_IP.setDocument(new IpDocument());
    DocumentListener listener = new DocumentListener()
    {
      public void removeUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void insertUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void changedUpdate(DocumentEvent event)
      {
        if(validateIP(jTF_IP.getText()))
        {
          jLblIp.setText("V");
          jLblIp.setForeground(Color.green);
          iStateButtonWrite|=c_iIP;
        }
        else
        {
          jLblIp.setText("!");
          jLblIp.setForeground(Color.red);
          iStateButtonWrite&=~c_iIP;
          
        }
        EnableButtonWriteIfPossible();
      }
    };
    jTF_IP.getDocument().addDocumentListener(listener);

    jTF_YealinkIP.setDocument(new IpDocument());
    DocumentListener listenerYealink = new DocumentListener()
    {
      public void removeUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void insertUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void changedUpdate(DocumentEvent event)
      {
        if(validateIP(jTF_YealinkIP.getText()))
        {
          jLblYealinkIp.setText("V");
          jLblYealinkIp.setForeground(Color.green);
          iStateButtonWrite|=c_iIP;
        }
        else
        {
          jLblYealinkIp.setText("!");
          jLblYealinkIp.setForeground(Color.red);
          iStateButtonWrite&=~c_iIP;
          
        }
        EnableButtonWriteIfPossible();
      }
    };
    jTF_YealinkIP.getDocument().addDocumentListener(listenerYealink);

  }

  void PortInputCfg()
  {
    jTF_Port.setDocument(new PortDocument());
    DocumentListener listenerTCP = new DocumentListener()
    {
      public void removeUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void insertUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void changedUpdate(DocumentEvent event)
      {
        String strPort=jTF_Port.getText();
        int iLen=strPort.length();
        if((iLen>0) && (iLen<6) && (Integer.parseInt(strPort)>0))
        {
          jLblPort.setText("V");
          jLblPort.setForeground(Color.green);
          iStateButtonWrite|=c_iPort;
        }
        else
        {
          jLblPort.setText("!");
          jLblPort.setForeground(Color.red);
          iStateButtonWrite&=~c_iPort;
          
        }
        EnableButtonWriteIfPossible();
      }
    };
    jTF_Port.getDocument().addDocumentListener(listenerTCP);

    jTF_UdpPort.setDocument(new PortDocument());
    DocumentListener listenerUdp = new DocumentListener()
    {
      public void removeUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void insertUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void changedUpdate(DocumentEvent event)
      {
        String strPort=jTF_UdpPort.getText();
        int iLen=strPort.length();
        if((iLen>0) && (iLen<6) && (Integer.parseInt(strPort)>0))
        {
          jLblUdpPort.setText("V");
          jLblUdpPort.setForeground(Color.green);
          iStateButtonWrite|=c_iPort;
        }
        else
        {
          jLblUdpPort.setText("!");
          jLblUdpPort.setForeground(Color.red);
          iStateButtonWrite&=~c_iPort;
        }
        EnableButtonWriteIfPossible();
      }
    };
    jTF_UdpPort.getDocument().addDocumentListener(listenerUdp);

    jTF_YealinkPort.setDocument(new PortDocument());
    DocumentListener listenerYealink = new DocumentListener()
    {
      public void removeUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void insertUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void changedUpdate(DocumentEvent event)
      {
        String strPort=jTF_YealinkPort.getText();
        int iLen=strPort.length();
        if((iLen>0) && (iLen<6) && (Integer.parseInt(strPort)>0))
        {
          jLblYealinkPort.setText("V");
          jLblYealinkPort.setForeground(Color.green);
          iStateButtonWrite|=c_iPort;
        }
        else
        {
          jLblYealinkPort.setText("!");
          jLblYealinkPort.setForeground(Color.red);
          iStateButtonWrite&=~c_iPort;
        }
        EnableButtonWriteIfPossible();
      }
    };
    jTF_YealinkPort.getDocument().addDocumentListener(listenerYealink);

  }

  void MaskInputCfg()
  {
    jTF_Mask.setDocument(new IpDocument());

    DocumentListener listener = new DocumentListener()
    {
      public void removeUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void insertUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void changedUpdate(DocumentEvent event)
      {
        if(validateIP(jTF_Mask.getText()))
        {
          jLblMask.setText("V");
          jLblMask.setForeground(Color.green);
          iStateButtonWrite|=c_iMask;
        }
        else
        {
          jLblMask.setText("!");
          jLblMask.setForeground(Color.red);
          iStateButtonWrite&=~c_iMask;
          
        }
        EnableButtonWriteIfPossible();
      }
    };
    jTF_Mask.getDocument().addDocumentListener(listener);
  }

  void GateInputCfg()
  {
    jTF_Gate.setDocument(new IpDocument());

    DocumentListener listener = new DocumentListener()
    {
      public void removeUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void insertUpdate(DocumentEvent event)
      { changedUpdate(event); }
      public void changedUpdate(DocumentEvent event)
      {
        if(validateIP(jTF_Gate.getText()))
        {
          jLblGate.setText("V");
          jLblGate.setForeground(Color.green);
          iStateButtonWrite|=c_iGate;
        }
        else
        {
          jLblGate.setText("!");
          jLblGate.setForeground(Color.red);
          iStateButtonWrite&=~c_iGate;
          
        }
        EnableButtonWriteIfPossible();
      }
    };
    jTF_Gate.getDocument().addDocumentListener(listener);
  }

  void EnableButtonWriteIfPossible()
  {
    int iState=iStateButtonWrite & (c_iIP|c_iPort|c_iMask|c_iGate);

    if(iState==(c_iIP|c_iPort|c_iMask|c_iGate))
    { jButtonWrite.setEnabled(true); }
    else jButtonWrite.setEnabled(false);
  }

  boolean validateIP(String ip)
  {
    Matcher IpMatcher = IpPattern.matcher(ip);
    return IpMatcher.matches();
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents()
  {

    jDlgTestTCP = new javax.swing.JDialog();
    jBtnSendCmdMicStatus = new javax.swing.JButton();
    jScrollPane2 = new javax.swing.JScrollPane();
    jTextAreaTCP = new javax.swing.JTextArea();
    jBtnConnect = new javax.swing.JButton();
    jBtnReset = new javax.swing.JButton();
    jLabel20 = new javax.swing.JLabel();
    jTabbedPane = new javax.swing.JTabbedPane();
    jPanel3 = new javax.swing.JPanel();
    jButtonSavePreset = new javax.swing.JButton();
    jLabel10 = new javax.swing.JLabel();
    jCB_VCam = new javax.swing.JComboBox<>();
    jLabel11 = new javax.swing.JLabel();
    jCB_VCamProto = new javax.swing.JComboBox<>();
    jLabel12 = new javax.swing.JLabel();
    jCB_VCamIF = new javax.swing.JComboBox<>();
    jLabel13 = new javax.swing.JLabel();
    jCB_VCamSpeed = new javax.swing.JComboBox<>();
    jButtonCallPreset = new javax.swing.JButton();
    jButtonDelPreset = new javax.swing.JButton();
    VCamCanvas = new java.awt.Canvas();
    jLabel14 = new javax.swing.JLabel();
    jTF_VCamPreset = new javax.swing.JTextField();
    jCB_VCamAddr = new javax.swing.JComboBox<>();
    jTB_PTZ = new javax.swing.JToolBar();
    jBt_Left = new javax.swing.JButton();
    jBt_Rigth = new javax.swing.JButton();
    jSeparator1 = new javax.swing.JToolBar.Separator();
    jBt_Up = new javax.swing.JButton();
    jBt_Down = new javax.swing.JButton();
    jSeparator2 = new javax.swing.JToolBar.Separator();
    jBt_ZoomP = new javax.swing.JButton();
    jBt_ZoomM = new javax.swing.JButton();
    jBt_VCamSetting = new javax.swing.JButton();
    jCheckBox_Rotate = new javax.swing.JCheckBox();
    jCheckBox_Mirror = new javax.swing.JCheckBox();
    jPanel2 = new javax.swing.JPanel();
    jScrollPane1 = new javax.swing.JScrollPane();
    jTableMicVCS = new javax.swing.JTable();
    jLabel6 = new javax.swing.JLabel();
    jCB_Proto = new javax.swing.JComboBox<>();
    jLabel7 = new javax.swing.JLabel();
    jCB_IF = new javax.swing.JComboBox<>();
    jLabel8 = new javax.swing.JLabel();
    jCB_Speed = new javax.swing.JComboBox<>();
    jLabel15 = new javax.swing.JLabel();
    jCB_CamDef = new javax.swing.JComboBox<>();
    jScrollPane3 = new javax.swing.JScrollPane();
    jTableVCamSN = new javax.swing.JTable();
    jLabel21 = new javax.swing.JLabel();
    jBtnM500Connected = new javax.swing.JButton();
    jScrollPane4 = new javax.swing.JScrollPane();
    jTxtM500Result = new javax.swing.JTextArea();
    jPanel1 = new javax.swing.JPanel();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jTF_Port = new javax.swing.JTextField();
    jTF_IP = new javax.swing.JTextField();
    jLblIp = new javax.swing.JLabel();
    jTF_Mask = new javax.swing.JTextField();
    jLblMask = new javax.swing.JLabel();
    jTF_Gate = new javax.swing.JTextField();
    jLblGate = new javax.swing.JLabel();
    jLblPort = new javax.swing.JLabel();
    jLabel9 = new javax.swing.JLabel();
    jCB_PM2Speed = new javax.swing.JComboBox<>();
    jBtnTestTCP = new javax.swing.JButton();
    jLabel16 = new javax.swing.JLabel();
    jTF_UdpPort = new javax.swing.JTextField();
    jLblUdpPort = new javax.swing.JLabel();
    jLabel17 = new javax.swing.JLabel();
    jTF_YealinkIP = new javax.swing.JTextField();
    jLblYealinkIp = new javax.swing.JLabel();
    jLabel18 = new javax.swing.JLabel();
    jTF_YealinkPort = new javax.swing.JTextField();
    jLblYealinkPort = new javax.swing.JLabel();
    jLabel19 = new javax.swing.JLabel();
    jButtonWrite = new javax.swing.JButton();
    jButtonExit = new javax.swing.JButton();
    jComboBoxRS232 = new javax.swing.JComboBox<>();
    jLabel5 = new javax.swing.JLabel();

    jDlgTestTCP.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    jDlgTestTCP.setTitle("�������� ������ �� TCP/IP");
    jDlgTestTCP.setModal(true);
    jDlgTestTCP.setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
    jDlgTestTCP.setName(""); // NOI18N
    jDlgTestTCP.setUndecorated(true);
    jDlgTestTCP.setResizable(false);
    jDlgTestTCP.setSize(new java.awt.Dimension(408, 340));

    jBtnSendCmdMicStatus.setText("�������� ������� <help mic_status>");
    jBtnSendCmdMicStatus.setEnabled(false);
    jBtnSendCmdMicStatus.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBtnSendCmdMicStatusActionPerformed(evt);
      }
    });

    jTextAreaTCP.setEditable(false);
    jTextAreaTCP.setColumns(20);
    jTextAreaTCP.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
    jTextAreaTCP.setRows(5);
    jTextAreaTCP.setFocusable(false);
    jScrollPane2.setViewportView(jTextAreaTCP);

    jBtnConnect.setText("����������� � ���");
    jBtnConnect.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBtnConnectActionPerformed(evt);
      }
    });

    jBtnReset.setText("��������� ����������");
    jBtnReset.setEnabled(false);
    jBtnReset.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBtnResetActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jDlgTestTCPLayout = new javax.swing.GroupLayout(jDlgTestTCP.getContentPane());
    jDlgTestTCP.getContentPane().setLayout(jDlgTestTCPLayout);
    jDlgTestTCPLayout.setHorizontalGroup(
      jDlgTestTCPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jDlgTestTCPLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jDlgTestTCPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jScrollPane2)
          .addGroup(jDlgTestTCPLayout.createSequentialGroup()
            .addComponent(jBtnConnect)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jBtnReset))
          .addGroup(jDlgTestTCPLayout.createSequentialGroup()
            .addComponent(jBtnSendCmdMicStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 291, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 97, Short.MAX_VALUE)))
        .addContainerGap())
    );
    jDlgTestTCPLayout.setVerticalGroup(
      jDlgTestTCPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jDlgTestTCPLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jDlgTestTCPLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jBtnConnect)
          .addComponent(jBtnReset))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jBtnSendCmdMicStatus)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(51, Short.MAX_VALUE))
    );

    jDlgTestTCP.getAccessibleContext().setAccessibleName("");
    jDlgTestTCP.getAccessibleContext().setAccessibleDescription("");

    jLabel20.setText("jLabel20");

    setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
    setTitle("��������� ��������� �C�");
    setResizable(false);
    addWindowListener(new java.awt.event.WindowAdapter()
    {
      public void windowClosing(java.awt.event.WindowEvent evt)
      {
        formWindowClosing(evt);
      }
    });

    jTabbedPane.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
    jTabbedPane.setPreferredSize(new java.awt.Dimension(520, 380));
    jTabbedPane.addChangeListener(new javax.swing.event.ChangeListener()
    {
      public void stateChanged(javax.swing.event.ChangeEvent evt)
      {
        jTabbedPaneStateChanged(evt);
      }
    });

    jButtonSavePreset.setText("���������");
    jButtonSavePreset.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jButtonSavePresetActionPerformed(evt);
      }
    });

    jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jLabel10.setText("������:");

    jCB_VCam.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

    jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jLabel11.setText("��������/�����:");

    jCB_VCamProto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jCB_VCamProto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pelco-D", "Visca" }));

    jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jLabel12.setText("������ �:");

    jCB_VCamIF.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

    jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jLabel13.setText("��������:");

    jCB_VCamSpeed.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jCB_VCamSpeed.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2400", "9600", "115200" }));

    jButtonCallPreset.setText("����������");
    jButtonCallPreset.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jButtonCallPresetActionPerformed(evt);
      }
    });

    jButtonDelPreset.setText("�������");
    jButtonDelPreset.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jButtonDelPresetActionPerformed(evt);
      }
    });

    VCamCanvas.setMinimumSize(new java.awt.Dimension(358, 232));
    VCamCanvas.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseClicked(java.awt.event.MouseEvent evt)
      {
        VCamCanvasMouseClicked(evt);
      }
    });

    jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jLabel14.setText("��������:");

    jTF_VCamPreset.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jTF_VCamPreset.setText("0");
    jTF_VCamPreset.setAutoscrolls(false);

    jCB_VCamAddr.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    jCB_VCamAddr.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7" }));

    jTB_PTZ.setBorder(null);
    jTB_PTZ.setFloatable(false);
    jTB_PTZ.setOrientation(javax.swing.SwingConstants.VERTICAL);
    jTB_PTZ.setRollover(true);
    jTB_PTZ.setBorderPainted(false);

    jBt_Left.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    jBt_Left.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/Left.gif"))); // NOI18N
    jBt_Left.setBorder(null);
    jBt_Left.setBorderPainted(false);
    jBt_Left.setContentAreaFilled(false);
    jBt_Left.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    jBt_Left.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/LeftP.gif"))); // NOI18N
    jBt_Left.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/LeftF.gif"))); // NOI18N
    jBt_Left.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/LeftF.gif"))); // NOI18N
    jBt_Left.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    jBt_Left.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mousePressed(java.awt.event.MouseEvent evt)
      {
        jBt_LeftMousePressed(evt);
      }
      public void mouseReleased(java.awt.event.MouseEvent evt)
      {
        jBt_LeftMouseReleased(evt);
      }
    });
    jBt_Left.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBt_LeftActionPerformed(evt);
      }
    });
    jTB_PTZ.add(jBt_Left);

    jBt_Rigth.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    jBt_Rigth.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/Rigth.gif"))); // NOI18N
    jBt_Rigth.setBorder(null);
    jBt_Rigth.setBorderPainted(false);
    jBt_Rigth.setContentAreaFilled(false);
    jBt_Rigth.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    jBt_Rigth.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/RigthP.gif"))); // NOI18N
    jBt_Rigth.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/RigthF.gif"))); // NOI18N
    jBt_Rigth.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    jBt_Rigth.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mousePressed(java.awt.event.MouseEvent evt)
      {
        jBt_RigthMousePressed(evt);
      }
      public void mouseReleased(java.awt.event.MouseEvent evt)
      {
        jBt_RigthMouseReleased(evt);
      }
    });
    jBt_Rigth.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBt_RigthActionPerformed(evt);
      }
    });
    jTB_PTZ.add(jBt_Rigth);
    jTB_PTZ.add(jSeparator1);

    jBt_Up.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    jBt_Up.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/Up.gif"))); // NOI18N
    jBt_Up.setBorder(null);
    jBt_Up.setBorderPainted(false);
    jBt_Up.setContentAreaFilled(false);
    jBt_Up.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    jBt_Up.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/UpP.gif"))); // NOI18N
    jBt_Up.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/UpF.gif"))); // NOI18N
    jBt_Up.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/UpF.gif"))); // NOI18N
    jBt_Up.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    jBt_Up.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mousePressed(java.awt.event.MouseEvent evt)
      {
        jBt_UpMousePressed(evt);
      }
      public void mouseReleased(java.awt.event.MouseEvent evt)
      {
        jBt_UpMouseReleased(evt);
      }
    });
    jBt_Up.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBt_UpActionPerformed(evt);
      }
    });
    jTB_PTZ.add(jBt_Up);

    jBt_Down.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/Down.gif"))); // NOI18N
    jBt_Down.setBorder(null);
    jBt_Down.setBorderPainted(false);
    jBt_Down.setContentAreaFilled(false);
    jBt_Down.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    jBt_Down.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/DownP.gif"))); // NOI18N
    jBt_Down.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/DownF.gif"))); // NOI18N
    jBt_Down.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/DownF.gif"))); // NOI18N
    jBt_Down.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    jBt_Down.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mousePressed(java.awt.event.MouseEvent evt)
      {
        jBt_DownMousePressed(evt);
      }
      public void mouseReleased(java.awt.event.MouseEvent evt)
      {
        jBt_DownMouseReleased(evt);
      }
    });
    jBt_Down.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBt_DownActionPerformed(evt);
      }
    });
    jTB_PTZ.add(jBt_Down);

    jSeparator2.setOpaque(true);
    jTB_PTZ.add(jSeparator2);

    jBt_ZoomP.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    jBt_ZoomP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomP.gif"))); // NOI18N
    jBt_ZoomP.setBorder(null);
    jBt_ZoomP.setBorderPainted(false);
    jBt_ZoomP.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    jBt_ZoomP.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomPP.gif"))); // NOI18N
    jBt_ZoomP.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomPF.gif"))); // NOI18N
    jBt_ZoomP.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomPF.gif"))); // NOI18N
    jBt_ZoomP.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    jBt_ZoomP.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mousePressed(java.awt.event.MouseEvent evt)
      {
        jBt_ZoomPMousePressed(evt);
      }
      public void mouseReleased(java.awt.event.MouseEvent evt)
      {
        jBt_ZoomPMouseReleased(evt);
      }
    });
    jBt_ZoomP.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBt_ZoomPActionPerformed(evt);
      }
    });
    jTB_PTZ.add(jBt_ZoomP);

    jBt_ZoomM.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    jBt_ZoomM.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomM.gif"))); // NOI18N
    jBt_ZoomM.setBorder(null);
    jBt_ZoomM.setBorderPainted(false);
    jBt_ZoomM.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    jBt_ZoomM.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomMP.gif"))); // NOI18N
    jBt_ZoomM.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomMF.gif"))); // NOI18N
    jBt_ZoomM.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/pm2_vcs_cfg/ZoomMF.gif"))); // NOI18N
    jBt_ZoomM.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    jBt_ZoomM.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mousePressed(java.awt.event.MouseEvent evt)
      {
        jBt_ZoomMMousePressed(evt);
      }
      public void mouseReleased(java.awt.event.MouseEvent evt)
      {
        jBt_ZoomMMouseReleased(evt);
      }
    });
    jBt_ZoomM.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBt_ZoomMActionPerformed(evt);
      }
    });
    jTB_PTZ.add(jBt_ZoomM);

    jBt_VCamSetting.setText("��������� ������");
    jBt_VCamSetting.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBt_VCamSettingActionPerformed(evt);
      }
    });

    jCheckBox_Rotate.setText("�����������");
    jCheckBox_Rotate.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jCheckBox_RotateActionPerformed(evt);
      }
    });

    jCheckBox_Mirror.setText("���������");
    jCheckBox_Mirror.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jCheckBox_MirrorActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel3Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jTF_VCamPreset, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jButtonCallPreset)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jButtonSavePreset)
            .addGap(8, 8, 8))
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addComponent(jTB_PTZ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(VCamCanvas, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
              .addComponent(jCB_VCam, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jCB_VCamProto, 0, 85, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addComponent(jCB_VCamAddr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
              .addComponent(jBt_VCamSetting, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                  .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                  .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                  .addComponent(jCB_VCamIF, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(jCB_VCamSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
              .addComponent(jCheckBox_Rotate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jCheckBox_Mirror, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(0, 0, Short.MAX_VALUE))
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addComponent(jButtonDelPreset)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel3Layout.createSequentialGroup()
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addComponent(jLabel10)
            .addGap(8, 8, 8)
            .addComponent(jCB_VCam, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(4, 4, 4)
            .addComponent(jLabel11)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jCB_VCamProto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jCB_VCamAddr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel14)
              .addComponent(jCB_VCamIF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel13)
              .addComponent(jCB_VCamSpeed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jBt_VCamSetting)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jCheckBox_Rotate)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jCheckBox_Mirror)
            .addGap(25, 25, 25))
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(VCamCanvas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jTB_PTZ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jButtonCallPreset)
          .addComponent(jButtonSavePreset)
          .addComponent(jLabel12)
          .addComponent(jTF_VCamPreset, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jButtonDelPreset))
        .addContainerGap())
    );

    jTabbedPane.addTab("��������� ��������", jPanel3);

    jTableMicVCS.setAutoCreateRowSorter(true);
    jTableMicVCS.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][]
      {
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null},
        {null, null, null}
      },
      new String []
      {
        "PM-2", "� ������", "� �������"
      }
    )
    {
      Class[] types = new Class []
      {
        java.lang.Byte.class, java.lang.String.class, java.lang.String.class
      };
      boolean[] canEdit = new boolean []
      {
        false, true, true
      };

      public Class getColumnClass(int columnIndex)
      {
        return types [columnIndex];
      }

      public boolean isCellEditable(int rowIndex, int columnIndex)
      {
        return canEdit [columnIndex];
      }
    });
    jTableMicVCS.setCellSelectionEnabled(true);
    jTableMicVCS.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jTableMicVCS.getTableHeader().setResizingAllowed(false);
    jTableMicVCS.getTableHeader().setReorderingAllowed(false);
    jTableMicVCS.setUpdateSelectionOnSort(false);
    jTableMicVCS.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseClicked(java.awt.event.MouseEvent evt)
      {
        jTableMicVCSMouseClicked(evt);
      }
    });
    jScrollPane1.setViewportView(jTableMicVCS);
    jTableMicVCS.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    if (jTableMicVCS.getColumnModel().getColumnCount() > 0)
    {
      jTableMicVCS.getColumnModel().getColumn(0).setResizable(false);
      jTableMicVCS.getColumnModel().getColumn(1).setResizable(false);
      jTableMicVCS.getColumnModel().getColumn(1).setCellEditor(jTableMicVCS.getCellEditor());
      jTableMicVCS.getColumnModel().getColumn(2).setResizable(false);
      jTableMicVCS.getColumnModel().getColumn(2).setCellEditor(jTableMicVCS.getCellEditor());
    }

    jLabel6.setText("��������:");

    jCB_Proto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pelco-D", "Visca" }));

    jLabel7.setText("���������:");

    jCB_IF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "RS-232", "RS-485" }));

    jLabel8.setText("��������:");

    jCB_Speed.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1200", "2400", "4800", "9600" }));

    jLabel15.setText("������ �� ���������:");

    jCB_CamDef.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " ", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15" }));

    jTableVCamSN.setModel(new javax.swing.table.DefaultTableModel(
      new Object [][]
      {
        {null, null},
        {null, null},
        {null, null}
      },
      new String []
      {
        "������", "SN"
      }
    )
    {
      Class[] types = new Class []
      {
        java.lang.String.class, java.lang.String.class
      };

      public Class getColumnClass(int columnIndex)
      {
        return types [columnIndex];
      }
    });
    jTableVCamSN.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
    jTableVCamSN.setAutoscrolls(false);
    jTableVCamSN.setCellSelectionEnabled(true);
    jTableVCamSN.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jTableVCamSN.addMouseListener(new java.awt.event.MouseAdapter()
    {
      public void mouseClicked(java.awt.event.MouseEvent evt)
      {
        jTableVCamSNMouseClicked(evt);
      }
    });
    jScrollPane3.setViewportView(jTableVCamSN);

    jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    jLabel21.setText("Yealink M500");

    jBtnM500Connected.setText("������������ ����������");
    jBtnM500Connected.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBtnM500ConnectedActionPerformed(evt);
      }
    });

    jTxtM500Result.setEditable(false);
    jTxtM500Result.setColumns(20);
    jTxtM500Result.setRows(5);
    jScrollPane4.setViewportView(jTxtM500Result);

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel2Layout.createSequentialGroup()
              .addComponent(jLabel6)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(jCB_Proto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
              .addComponent(jLabel7)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(jCB_IF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(jLabel15)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jCB_CamDef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jCB_Speed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
              .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE)))
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addGap(32, 32, 32)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(jBtnM500Connected, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
        .addContainerGap(26, Short.MAX_VALUE))
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
        .addGap(3, 3, 3)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel6)
          .addComponent(jCB_Proto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel7)
          .addComponent(jCB_IF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel8)
          .addComponent(jCB_Speed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel15)
          .addComponent(jCB_CamDef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jBtnM500Connected)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 177, Short.MAX_VALUE))
          .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
        .addContainerGap())
    );

    jTabbedPane.addTab("������ � �������", jPanel2);

    jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel1.setText("IP-����� ���:");

    jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel2.setText("������ ��� TCP-IP ����:");

    jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel3.setText("����� ����:");

    jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel4.setText("����:");

    jTF_Port.setAutoscrolls(false);

    jLblIp.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
    jLblIp.setForeground(new java.awt.Color(0, 255, 0));
    jLblIp.setText("V");

    jLblMask.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
    jLblMask.setForeground(new java.awt.Color(0, 255, 0));
    jLblMask.setText("V");

    jLblGate.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
    jLblGate.setForeground(new java.awt.Color(0, 255, 0));
    jLblGate.setText("V");

    jLblPort.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
    jLblPort.setForeground(new java.awt.Color(0, 255, 0));
    jLblPort.setText("V");

    jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel9.setText("�������� ������ � ��-2:");

    jCB_PM2Speed.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2400", "4800", "9600", "19200", "38400", "57600", "115200", "230400" }));

    jBtnTestTCP.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jBtnTestTCP.setText("��������� TCP/IP...");
    jBtnTestTCP.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jBtnTestTCPActionPerformed(evt);
      }
    });

    jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel16.setText("UDP ����:");

    jTF_UdpPort.setAutoscrolls(false);

    jLblUdpPort.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
    jLblUdpPort.setForeground(new java.awt.Color(0, 255, 0));
    jLblUdpPort.setText("V");

    jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel17.setText("IP-�����:");

    jLblYealinkIp.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
    jLblYealinkIp.setForeground(new java.awt.Color(0, 255, 0));
    jLblYealinkIp.setText("V");

    jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel18.setText("TCP-IP ����:");

    jTF_YealinkPort.setAutoscrolls(false);

    jLblYealinkPort.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
    jLblYealinkPort.setForeground(new java.awt.Color(0, 255, 0));
    jLblYealinkPort.setText("V");

    jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    jLabel19.setText("Yealink MeetingEye");

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel3)
          .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
            .addGroup(jPanel1Layout.createSequentialGroup()
              .addComponent(jTF_Gate, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(jLblGate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
              .addComponent(jTF_Port, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(jLblPort, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jLabel4)
            .addComponent(jLabel16)
            .addGroup(jPanel1Layout.createSequentialGroup()
              .addComponent(jTF_UdpPort, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(jLblUdpPort, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jTF_Mask, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jLblMask, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel1)
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jTF_IP, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblIp, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(18, 18, 18)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jCB_PM2Speed, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jBtnTestTCP, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGap(39, 39, 39))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jTF_YealinkPort, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblYealinkPort, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jTF_YealinkIP, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLblYealinkIp, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addGap(25, 25, 25)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel1)
          .addComponent(jLabel19)
          .addComponent(jLabel9))
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLblIp, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jTF_IP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jLabel3)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
              .addComponent(jTF_Mask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jLblMask, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTF_Gate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
              .addComponent(jLblGate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(18, 18, 18)
            .addComponent(jLabel2)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLblPort, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jTF_Port, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
              .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel16)
                .addGap(26, 26, 26))
              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLblUdpPort, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jTF_UdpPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(77, 77, 77))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(6, 6, 6)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel17)
              .addComponent(jCB_PM2Speed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(1, 1, 1)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLblYealinkIp, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jTF_YealinkIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jLabel18)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jTF_YealinkPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(jLblYealinkPort, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jBtnTestTCP, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())))
    );

    jTabbedPane.addTab("���������", jPanel1);

    jButtonWrite.setText("�������� � �C�");
    jButtonWrite.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        Store2ControlVCamUnit(evt);
      }
    });

    jButtonExit.setText("�����");
    jButtonExit.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(java.awt.event.ActionEvent evt)
      {
        jButtonExitActionPerformed(evt);
      }
    });

    jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel5.setText("COM-����:");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jComboBoxRS232, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(jButtonWrite)
        .addGap(90, 90, 90)
        .addComponent(jButtonExit)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
      .addComponent(jTabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 597, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(jTabbedPane, javax.swing.GroupLayout.PREFERRED_SIZE, 407, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jButtonWrite)
          .addComponent(jButtonExit)
          .addComponent(jComboBoxRS232, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel5))
        .addGap(12, 12, 12))
    );

    pack();
  }// </editor-fold>//GEN-END:initComponents

  String GetIpFromInp(String strIpInp)
  {
    int i,iDec;
    String strIP="";

    strIpInp=strIpInp.replace('.',':');
    String [] strArr=strIpInp.split(":");
    for(i=0;i<strArr.length;i++)
    {
      iDec=Integer.parseInt(strArr[i]);
      strIP=strIP+String.format("%02X",iDec);
    }
    return strIP;
  }

//                                                         Yealink VCS
//               IP   ,TCPp,  Mask  ,  Gate  ,Speed ,UDPp,   Ip   ,TCPp
// PM2_VCS+IP:C0A80164,17AC,FFFFFF00,00000000,01C200,17AD,C0A80164,17AE,0dh
  String CreateIpCfgString()
  {
    String strIP;
    int iVal;

    strIP="PM2_VCS+IP:"+GetIpFromInp(jTF_IP.getText())+",";
    iVal=Integer.parseInt(jTF_Port.getText());
    strIP=strIP+String.format("%04X,",iVal);
    strIP=strIP+GetIpFromInp(jTF_Mask.getText())+",";
    strIP=strIP+GetIpFromInp(jTF_Gate.getText())+",";
    iVal=Integer.parseInt((String)(jCB_PM2Speed.getSelectedItem()));
    strIP=strIP+String.format("%06X,",iVal);
    iVal=Integer.parseInt(jTF_UdpPort.getText());
    strIP=strIP+String.format("%04X,",iVal);
    strIP=strIP+GetIpFromInp(jTF_YealinkIP.getText())+",";
    iVal=Integer.parseInt(jTF_YealinkPort.getText());
    strIP=strIP+String.format("%04X\r",iVal);
    return strIP;
  }

  int GetIntCellValue(JTable jTab,int iRow,int iCol)
  {
    Object obj=jTab.getValueAt(iRow,iCol);
    if(obj==null) return 0;
    String strVal=(String)obj;
    strVal=strVal.trim();
    if(strVal.isEmpty()) return 0;
    return Integer.parseInt(strVal);
  }

  String GetStringCellValue(JTable jTab,int iRow,int iCol)
  {
    Object obj=jTab.getValueAt(iRow,iCol);
    if(obj==null) return "";
    String strVal=(String)obj;
    strVal=strVal.trim();
    return strVal;
  }

// 
//           VCam    VCamSN
// PM2_VCS+SN:01-01234567890123456,...
  String CreateVCamSNString()
  {
    int i,iVCam;
    String strSN;
    String strVCamSN="";

    for(i=0;i<jTableVCamSN.getRowCount();i++)
    {
      if(!strVCamSN.isEmpty()) strVCamSN+=",";
      iVCam=GetIntCellValue(jTableVCamSN,i,0);
      strSN=GetStringCellValue(jTableVCamSN,i,1);
      if((iVCam==0) || strSN.isEmpty()) strVCamSN+="00-0000000000000000";
      else strVCamSN+=String.format("%02X-",iVCam)+strSN;
    }
    strVCamSN="PM2_VCS+SN:"+strVCamSN+"\r";
    return strVCamSN;
  }

// PM2_VCS+MIC:ProtoIF,MicVCamPreset,...
// PM2_VCS+MIC:00,010101,...,0dh
// ProtoIF: b11..b8 - Cam def, b7..b4 - Protocol, b3..b2 - Speed, b1..b0 - Interface
// MicVCamPreset: b23..b16 - Mic, b15..b8 - VCam, b7..b0 - Preset
  String CreateMicVCamCfgString()
  {
    int i,iMic,iVCam,iPreset;
    String strMicCfg="PM2_VCS+MIC:";

    strMicCfg=strMicCfg+String.format("%04X ",
              (jCB_CamDef.getSelectedIndex() << 8) |
              (jCB_Proto.getSelectedIndex() << 4) |
              (jCB_Speed.getSelectedIndex() << 2) |
              jCB_IF.getSelectedIndex());
    for(i=0;i<jTableMicVCS.getRowCount();i++)
    {
      iVCam=GetIntCellValue(jTableMicVCS,i,1);
      iPreset=GetIntCellValue(jTableMicVCS,i,2);
      if((iVCam>0) && (iVCam<16) &&
         (iPreset>0) && (iPreset<256))
      {
        iMic=((i+1)<<16) | (iVCam << 8) | iPreset;
        strMicCfg=strMicCfg+String.format("%06X ",iMic);
      }
    }
    strMicCfg=strMicCfg.trim();
    strMicCfg=strMicCfg.replace(' ',',')+"\r";
    return strMicCfg;
  }


  String GetIpFromCfg(String strIp)
  {
    int i,iHex;
    String strIpInp="",strHex;

    for(i=0;i<4;i++)
    {
      strHex=strIp.substring(i*2,i*2+2);
      iHex=Integer.valueOf(strHex,16);
      if(i<3) strIpInp=strIpInp+Integer.toString(iHex)+".";
      else strIpInp=strIpInp+Integer.toString(iHex);
    }
    return strIpInp;
  }

  String GetPortCfg(String strPort)
  {
    int i,iHex;
    String strHex;

    strHex=strPort.substring(0,2);
    iHex=Integer.valueOf(strHex,16);
    strHex=strPort.substring(2,4);
    iHex=(iHex << 8) | Integer.valueOf(strHex,16);
    return Integer.toString(iHex);
  }

  String GetPM2SpeedFromCfg(String strSpeed)
  {
    int i,iHex;
    String strHex;

    strHex=strSpeed.substring(0,2);
    iHex=Integer.valueOf(strHex,16);
    strHex=strSpeed.substring(2,4);
    iHex=(iHex << 8) | Integer.valueOf(strHex,16);
    strHex=strSpeed.substring(4,6);
    iHex=(iHex << 8) | Integer.valueOf(strHex,16);
    return Integer.toString(iHex);
  }

  void SetValue2VCamSNTable(String strVCamSN)
  {
    int i,iRow,iVCam;

    strVCamSN=strVCamSN.replace(',',':');
    String[] strArr=strVCamSN.split(":");
    for(iRow=0;(iRow<3);iRow++)
    {
      jTableVCamSN.setValueAt("",iRow,0);
      jTableVCamSN.setValueAt("",iRow,1);
    }
    for(i=0,iRow=0;(i<strArr.length) && (iRow<3);i++)
    {
      if(strArr[i].charAt(2)=='-')
      {
        iVCam=Integer.parseInt(strArr[i].substring(0,2),10);
        if(iVCam>0)
        {
          jTableVCamSN.setValueAt(String.format("%02d",iVCam),iRow,0);
          jTableVCamSN.setValueAt(strArr[i].substring(3),iRow,1);
          iRow++;
        }
      }
    }
  }

// PM2_VCS+MIC:ProtoIF,MicVCamPreset,...
// PM2_VCS+MIC:00,010101,...,0dh
// ProtoIF: b11..b8 - Cam def, b7..b4 - Protocol, b3..b2 - Speed, b1..b0 - Interface
// MicVCamPreset: b23..b16 - Mic, b15..b8 - VCam, b7..b0 - Preset
  void SetValue2MicTable(String strMicVCam)
  {
    int i,iVal,iMic,iVCam,iPreset;

    strMicVCam=strMicVCam.replace(',',':');
    String [] strArr=strMicVCam.split(":");
    iVal=Integer.parseInt(strArr[0],16);
    jCB_CamDef.setSelectedIndex(iVal >> 8);
    jCB_Proto.setSelectedIndex((iVal >> 4) & 0x1);
    jCB_IF.setSelectedIndex(iVal & 0x03);
    jCB_Speed.setSelectedIndex((iVal >> 2) & 0x03);
    for(i=1;i<strArr.length;i++)
    {
      iVal=Integer.parseInt(strArr[i],16);
      iMic=(iVal >> 16)-1;
      iVCam=(iVal >> 8) & 0xff;
      iPreset=iVal & 0xff;
      jTableMicVCS.setValueAt(String.format("%02d",iVCam),iMic,1);
      jTableMicVCS.setValueAt(String.format("%d",iPreset),iMic,2);
    }
  }

  void ReadCfgFromFile()
  {
    boolean bOkIp=false;

    try
    {
      int iVal;
      InputStream ins = new FileInputStream(c_strFName);
      Reader r = new InputStreamReader(ins, "windows-1251");
      BufferedReader br = new BufferedReader(r);
      String s;
      ArrayList<String> stringsFromIniFile=new ArrayList<String>();

      // read from file
      while((s=br.readLine())!=null)
      {
        s=s.trim();
        if(s.length()>0) stringsFromIniFile.add(s);
      }
      br.close();
      r.close();
      ins.close();

      for(String sCfg : stringsFromIniFile)
      {
        if(sCfg.indexOf("PM2_VCS+IP:")==0)
        {
          jTF_IP.setText(GetIpFromCfg(sCfg.substring(11,19)));
          jTF_Port.setText(GetPortCfg(sCfg.substring(20,24)));
          jTF_Mask.setText(GetIpFromCfg(sCfg.substring(25,33)));
          jTF_Gate.setText(GetIpFromCfg(sCfg.substring(34,42)));
          jCB_PM2Speed.setSelectedItem(GetPM2SpeedFromCfg(sCfg.substring(43,49)));
          if(sCfg.length()>53)
          {
            jTF_UdpPort.setText(GetPortCfg(sCfg.substring(50,54)));
            jTF_YealinkIP.setText(GetIpFromCfg(sCfg.substring(55,63)));
            jTF_YealinkPort.setText(GetPortCfg(sCfg.substring(64,68)));
          }
          else
          {
            iVal=Integer.parseInt(jTF_Port.getText())+1;
            jTF_UdpPort.setText(String.format("%d",iVal));
            jTF_YealinkIP.setText("0.0.0.0");
            jTF_YealinkPort.setText("6024");
          }
          bOkIp=true;
        }
        else
        {
          if(sCfg.indexOf("PM2_VCS+SN:")==0)
          { SetValue2VCamSNTable(sCfg.substring(11)); }
          else
          {
            if(sCfg.indexOf("PM2_VCS+MIC:")==0)
            { SetValue2MicTable(sCfg.substring(12)); }
          }
        }
      }
      stringsFromIniFile.clear();
    }
    catch (Exception ex)
    { /*JOptionPane.showMessageDialog(null, ex.toString() + " ReadCfgFromFile()");*/ }
    if(!bOkIp)
    {
      jTF_IP.setText("192.168.1.150");
      jTF_Port.setText("3142");
      jTF_UdpPort.setText("3143");
      jTF_Mask.setText("255.255.255.0");
      jTF_Gate.setText("0.0.0.0");
    }
  }

  void SaveCfg2File(String strIpCfg,String strVCamSN,String strMicVCamCfg)
  {
    FileWriter fWr=null;

    try
    {
      fWr=new FileWriter(c_strFName,false);
      fWr.write(strIpCfg,0,strIpCfg.length());
      fWr.write(strVCamSN,0,strVCamSN.length());
      fWr.write(strMicVCamCfg,0,strMicVCamCfg.length());
    }
    catch(Exception fex)
    { System.out.println("SaveCfg2File: "+fex.toString()); }
    finally
    {
      if(fWr!=null)
      {
        try { fWr.close(); }
        catch(Exception ex) { }
      }
    }
  }

  private SerialPort openComPort()
  {
    int i;
    String strComPort=jComboBoxRS232.getItemAt(jComboBoxRS232.getSelectedIndex());
    for(i=0;i<MassRS232.length;i++)
    {
      if(strComPort.compareTo(MassRS232[i].getSystemPortName())==0)
      {
        SerialPort comPort=MassRS232[i];

        comPort.openPort();
        comPort.setComPortParameters(115200,8,SerialPort.ONE_STOP_BIT,SerialPort.NO_PARITY);
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING,100,0);
        return comPort;
      }
    }
    return null;
  }

  private void Store2ControlVCamUnit(java.awt.event.ActionEvent evt)//GEN-FIRST:event_Store2ControlVCamUnit
  {//GEN-HEADEREND:event_Store2ControlVCamUnit
    String strIpCfg=CreateIpCfgString();
    String strVCamSN=CreateVCamSNString();
    String strMicVCamCfg=CreateMicVCamCfgString();

    SaveCfg2File(strIpCfg,strVCamSN,strMicVCamCfg);
    if((iStateButtonWrite & c_iCOM)!=0)
    {
      {
        Object[] options = { "��", "���!" };
        int n = JOptionPane.showOptionDialog(this,
                               "�������� ������������ � �C�?",
                               "�������������",
                               JOptionPane.YES_NO_OPTION,
                               JOptionPane.QUESTION_MESSAGE,
                               null, options,options[0]);
        if(n==1) return;
      }
      SerialPort comPort=openComPort();
      if(comPort!=null)
      {
        boolean bOk=false;
        try
        {
          int iRd;
          byte[] rdBuf = new byte[7];

          comPort.writeBytes(strIpCfg.getBytes(),strIpCfg.length());
          iRd=comPort.readBytes(rdBuf, rdBuf.length);
          if((iRd==7) && (rdBuf[0]=='O') && (rdBuf[1]=='K'))
          {
            if(!strVCamSN.isEmpty())
            {
              comPort.writeBytes(strVCamSN.getBytes(),strVCamSN.length());
              // �� ����������� ����� ��� ������������� � ����������� ���������� ���!
              comPort.readBytes(rdBuf, rdBuf.length);
            }
            comPort.writeBytes(strMicVCamCfg.getBytes(),strMicVCamCfg.length());
            iRd=comPort.readBytes(rdBuf, rdBuf.length);
            if((iRd==7) && (rdBuf[0]=='O') && (rdBuf[1]=='K')) bOk=true;
          }
        }
        catch (Exception e)
        { e.printStackTrace(); }
        comPort.closePort();
        if(bOk)
        {
          String msg="<html><p align=\"center\">������������ ������� �������� � �C�!</p>";
          JOptionPane.showMessageDialog(this,msg,"��������",JOptionPane.WARNING_MESSAGE);
        }
        else
        {
          String msg="<html><p align=\"center\">������ ������ ������������ � �C�!</p>";
          JOptionPane.showMessageDialog(this,msg,"������",JOptionPane.ERROR_MESSAGE);
        }
      }
    }
  }//GEN-LAST:event_Store2ControlVCamUnit

  private void jButtonExitActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonExitActionPerformed
  {//GEN-HEADEREND:event_jButtonExitActionPerformed
    ExitIfNeed();
  }//GEN-LAST:event_jButtonExitActionPerformed

  private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
  {//GEN-HEADEREND:event_formWindowClosing
    ExitIfNeed();
  }//GEN-LAST:event_formWindowClosing

  private void jButtonCallPresetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonCallPresetActionPerformed
  {//GEN-HEADEREND:event_jButtonCallPresetActionPerformed
    VCamMem_SendCmd(2);
  }//GEN-LAST:event_jButtonCallPresetActionPerformed

  private void jButtonDelPresetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonDelPresetActionPerformed
  {//GEN-HEADEREND:event_jButtonDelPresetActionPerformed
    VCamMem_SendCmd(0);
  }//GEN-LAST:event_jButtonDelPresetActionPerformed

  private void jButtonSavePresetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButtonSavePresetActionPerformed
  {//GEN-HEADEREND:event_jButtonSavePresetActionPerformed
    VCamMem_SendCmd(1);
  }//GEN-LAST:event_jButtonSavePresetActionPerformed

  private void VCamCanvasMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_VCamCanvasMouseClicked
  {//GEN-HEADEREND:event_VCamCanvasMouseClicked
    if(evt.getClickCount()==2)
    {
      if(grabber==null) return;
      if(cFrame==null)
      {
        VCamTimer.stop();
        cFrame= new CanvasFrameTS("PM2_VCS_CFG large size video view");
        try
        { cFrame.setIconImage(new ImageIcon("./PM2VCSCFG.gif").getImage()); }
        catch(Exception ex)
        { ex.printStackTrace(); }
        cFrame.setSize(grabber.getVCamDimension());
        cFrame.setLocation(0,0);
        grabber.setWidth(cFrame.getWidth());
        grabber.setHeight(cFrame.getHeight());
        try
        {
          String strInfo="VIEW LARGE SIZE VIDEO...";
          Graphics vcg= VCamCanvas.getGraphics();
          int iStrW=vcg.getFontMetrics().stringWidth(strInfo);
          vcg.clearRect(0, 0, VCamCanvas.getWidth(),VCamCanvas.getHeight());
          vcg.drawString(strInfo,
                         (VCamCanvas.getWidth()-iStrW)/2,VCamCanvas.getHeight()/2);
          vcg.dispose();
          cFrame.addWindowListener(new WindowAdapter()
          {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt)
            { StartGrabber(); }
          });
          VCamTimer.start();
        }
        catch(Exception e)
        {
          cFrame.dispose();
          cFrame=null;
          VCamTimer.start();
        }
      }
      else cFrame.setVisible(true);
    }
  }//GEN-LAST:event_VCamCanvasMouseClicked

  private void jBt_LeftActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBt_LeftActionPerformed
  {//GEN-HEADEREND:event_jBt_LeftActionPerformed
    VCamPTDrive_SendCmd(1,0);
  }//GEN-LAST:event_jBt_LeftActionPerformed

  private void jBt_RigthActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBt_RigthActionPerformed
  {//GEN-HEADEREND:event_jBt_RigthActionPerformed
    VCamPTDrive_SendCmd(-1,0);
  }//GEN-LAST:event_jBt_RigthActionPerformed

  private void jBt_UpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBt_UpActionPerformed
  {//GEN-HEADEREND:event_jBt_UpActionPerformed
    VCamPTDrive_SendCmd(0,1);
  }//GEN-LAST:event_jBt_UpActionPerformed

  private void jBt_DownActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBt_DownActionPerformed
  {//GEN-HEADEREND:event_jBt_DownActionPerformed
    VCamPTDrive_SendCmd(0,-1);
  }//GEN-LAST:event_jBt_DownActionPerformed

  private void jBt_ZoomPActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBt_ZoomPActionPerformed
  {//GEN-HEADEREND:event_jBt_ZoomPActionPerformed
    VCamZoom_SendCmd(1);
    VCamZoomTimer.start();
  }//GEN-LAST:event_jBt_ZoomPActionPerformed

  private void jBt_ZoomMActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBt_ZoomMActionPerformed
  {//GEN-HEADEREND:event_jBt_ZoomMActionPerformed
    VCamZoom_SendCmd(-1);
    VCamZoomTimer.start();
  }//GEN-LAST:event_jBt_ZoomMActionPerformed

  private void jBt_LeftMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_LeftMousePressed
  {//GEN-HEADEREND:event_jBt_LeftMousePressed
    m_iPan=1; m_iTilt=0;
    VCamPTDrive_SendCmd(m_iPan,m_iTilt);
    VCamPTTimer.start();
  }//GEN-LAST:event_jBt_LeftMousePressed

  private void jBt_LeftMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_LeftMouseReleased
  {//GEN-HEADEREND:event_jBt_LeftMouseReleased
    VCamPTTimer.stop();
  }//GEN-LAST:event_jBt_LeftMouseReleased

  private void jBt_RigthMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_RigthMousePressed
  {//GEN-HEADEREND:event_jBt_RigthMousePressed
    m_iPan=-1; m_iTilt=0;
    VCamPTDrive_SendCmd(m_iPan,m_iTilt);
    VCamPTTimer.start();
  }//GEN-LAST:event_jBt_RigthMousePressed

  private void jBt_RigthMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_RigthMouseReleased
  {//GEN-HEADEREND:event_jBt_RigthMouseReleased
    VCamPTTimer.stop();
  }//GEN-LAST:event_jBt_RigthMouseReleased

  private void jBt_UpMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_UpMousePressed
  {//GEN-HEADEREND:event_jBt_UpMousePressed
    m_iPan=0; m_iTilt=1;
    VCamPTDrive_SendCmd(m_iPan,m_iTilt);
    VCamPTTimer.start();

  }//GEN-LAST:event_jBt_UpMousePressed

  private void jBt_UpMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_UpMouseReleased
  {//GEN-HEADEREND:event_jBt_UpMouseReleased
    VCamPTTimer.stop();
  }//GEN-LAST:event_jBt_UpMouseReleased

  private void jBt_DownMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_DownMousePressed
  {//GEN-HEADEREND:event_jBt_DownMousePressed
    m_iPan=0; m_iTilt=-1;
    VCamPTDrive_SendCmd(m_iPan,m_iTilt);
    VCamPTTimer.start();
  }//GEN-LAST:event_jBt_DownMousePressed

  private void jBt_DownMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_DownMouseReleased
  {//GEN-HEADEREND:event_jBt_DownMouseReleased
    VCamPTTimer.stop();
  }//GEN-LAST:event_jBt_DownMouseReleased

  private void jBt_ZoomPMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_ZoomPMousePressed
  {//GEN-HEADEREND:event_jBt_ZoomPMousePressed
    VCamZoomTimer.stop();
    VCamZoom_SendCmd(1);
  }//GEN-LAST:event_jBt_ZoomPMousePressed

  private void jBt_ZoomPMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_ZoomPMouseReleased
  {//GEN-HEADEREND:event_jBt_ZoomPMouseReleased
    VCamZoom_SendCmd(0);
  }//GEN-LAST:event_jBt_ZoomPMouseReleased

  private void jBt_ZoomMMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_ZoomMMousePressed
  {//GEN-HEADEREND:event_jBt_ZoomMMousePressed
    VCamZoomTimer.stop();
    VCamZoom_SendCmd(-1);
  }//GEN-LAST:event_jBt_ZoomMMousePressed

  private void jBt_ZoomMMouseReleased(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jBt_ZoomMMouseReleased
  {//GEN-HEADEREND:event_jBt_ZoomMMouseReleased
    VCamZoom_SendCmd(0);
  }//GEN-LAST:event_jBt_ZoomMMouseReleased

  private void jBt_VCamSettingActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBt_VCamSettingActionPerformed
  {//GEN-HEADEREND:event_jBt_VCamSettingActionPerformed
    if(m_iWebCamInd>-1)
    {
      if(grabber!=null) grabber.showSettings();
    }
  }//GEN-LAST:event_jBt_VCamSettingActionPerformed

  private void jCheckBox_RotateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jCheckBox_RotateActionPerformed
  {//GEN-HEADEREND:event_jCheckBox_RotateActionPerformed
    if(grabber!=null)
    { grabber.setRotate(jCheckBox_Rotate.isSelected()); }
  }//GEN-LAST:event_jCheckBox_RotateActionPerformed

  private void jCheckBox_MirrorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jCheckBox_MirrorActionPerformed
  {//GEN-HEADEREND:event_jCheckBox_MirrorActionPerformed
    if(grabber!=null)
    { grabber.setMirror(jCheckBox_Mirror.isSelected()); }
  }//GEN-LAST:event_jCheckBox_MirrorActionPerformed

  private void jTabbedPaneStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_jTabbedPaneStateChanged
  {//GEN-HEADEREND:event_jTabbedPaneStateChanged
    boolean bEn=true;
    if(jPanel3.isShowing()) bEn=false;
    jButtonWrite.setEnabled(bEn);
    jComboBoxRS232.setEnabled(bEn);
  }//GEN-LAST:event_jTabbedPaneStateChanged

  private void JDlgTCPTimerEvent()
  {
    while(!m_bqIn.isEmpty())
    {
      XchgPckt pckt=m_bqIn.poll();
      if(jDlgTestTCP.isEnabled())
      {
        switch(pckt.iOp)
        {
          case 0: break;
          case 4: // ������� ������ �� ���
            if(pckt.iData==0) jTextAreaTCP.append(pckt.strData);
            break;
          case 11: // answ.connect
            if(pckt.iData==0)
            {
              jTextAreaTCP.append("���������� � ��� �����������!\r\n");
              jBtnConnect.setEnabled(false);
              jBtnReset.setEnabled(true);
              jBtnSendCmdMicStatus.setEnabled(true);
            }
            else
            {
              jTextAreaTCP.append(pckt.strData);
              jBtnConnect.setEnabled(true);
            }
            break;
          case 12: // answ.disconnect
            if(pckt.iData==0)
            {
              jTextAreaTCP.append("���������� � ��� ���������!\r\n");
            }
            else jTextAreaTCP.append(pckt.strData);
            jBtnConnect.setEnabled(true);
            break;
          case 13: // answ.write
            if(pckt.iData==0)
            { jTextAreaTCP.append("������� <help mic_status> ��������!\r\n"); }
            else jTextAreaTCP.append(pckt.strData);
            break;
          default: jTextAreaTCP.append(pckt.strData);
        }
      }
    }
  }

  private void jBtnTestTCPActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBtnTestTCPActionPerformed
  {//GEN-HEADEREND:event_jBtnTestTCPActionPerformed
    jDlgTestTCP.getRootPane().setWindowDecorationStyle(JRootPane.INFORMATION_DIALOG);
    int iX=pm2vcscfg.getX()+(pm2vcscfg.getWidth()-jDlgTestTCP.getWidth())/2;
    int iY=pm2vcscfg.getY()+(pm2vcscfg.getHeight()-jDlgTestTCP.getHeight())/2;
    jDlgTestTCP.setLocation(iX, iY);
    jTextAreaTCP.setText("");
    jBtnConnect.setEnabled(true);
    jBtnReset.setEnabled(false);
    jBtnSendCmdMicStatus.setEnabled(false);
    m_bqOut.clear(); m_bqIn.clear();
    
    SocketThread scktThr=new SocketThread("Socket thread",m_bqOut,m_bqIn,
                                          Integer.parseInt(jTF_YealinkPort.getText()));
    scktThr.start();
    JDlgTCPTimer.start();
    jDlgTestTCP.setVisible(true);
    JDlgTCPTimer.stop();
    scktThr.Exited();
  }//GEN-LAST:event_jBtnTestTCPActionPerformed

  private void jBtnConnectActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBtnConnectActionPerformed
  {//GEN-HEADEREND:event_jBtnConnectActionPerformed
    jTextAreaTCP.append("\r\n����������� � ���...\r\n");
    jBtnConnect.setEnabled(false);
    XchgPckt pckt=new XchgPckt();
    pckt.iOp=1;
    pckt.strData=jTF_IP.getText();
    pckt.iData=Integer.parseInt(jTF_Port.getText());
    m_bqOut.add(pckt);
  }//GEN-LAST:event_jBtnConnectActionPerformed

  private void jBtnResetActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBtnResetActionPerformed
  {//GEN-HEADEREND:event_jBtnResetActionPerformed
    jTextAreaTCP.append("��������� ���������� � ���...\r\n");
    jBtnReset.setEnabled(false);
    jBtnSendCmdMicStatus.setEnabled(false);
    XchgPckt pckt=new XchgPckt();
    pckt.iOp=2;
    m_bqOut.add(pckt);
  }//GEN-LAST:event_jBtnResetActionPerformed

  private void jBtnSendCmdMicStatusActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBtnSendCmdMicStatusActionPerformed
  {//GEN-HEADEREND:event_jBtnSendCmdMicStatusActionPerformed
    jTextAreaTCP.append("�������� ������� <help mic_status> ���...\r\n");
    XchgPckt pckt=new XchgPckt();
    pckt.iOp=3;
    pckt.strData="help mic_status\r\n";
    m_bqOut.add(pckt);
  }//GEN-LAST:event_jBtnSendCmdMicStatusActionPerformed

/*
Send: system get devices\r\n
Return: system get devices {"device-list": [{ "model":"UVC86", "firmware":"128.423.253.104",
"hardware":"263.0.19.0.3.0.36", "macaddress": "00:15:65:00:00:00",
"serialnumber":"506607D117000009", }, { "model":"UVC84", "firmware":"130.303.253.44",
"hardware":"261.0.5.10.43.0.58", "macaddress":"00: 24:13:00:00:00",
"serialnumber":"803032E070000031", } ] }\r\n
*/
  private void jBtnM500ConnectedActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jBtnM500ConnectedActionPerformed
  {//GEN-HEADEREND:event_jBtnM500ConnectedActionPerformed
    Socket sTcp;
    BufferedReader br;
    try
    {
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      sTcp = new Socket(jTF_YealinkIP.getText(),
                        Integer.parseInt(jTF_YealinkPort.getText()));
      sTcp.setSoTimeout(200);
      br=new BufferedReader(new InputStreamReader(sTcp.getInputStream()));
      Thread.sleep(200);
      br.readLine();
      sTcp.getOutputStream().write("system get devices\r\n".getBytes());
      Thread.sleep(200);
      String strData=br.readLine();
      if(strData!=null)
      {
        strData=strData.replace("{","\r\n{");
        jTxtM500Result.setText(strData+"\r\n");
      }
      else jTxtM500Result.setText("Yealink M500 �� ��������!\r\n");
      sTcp.close();
      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
    catch (Exception e) 
    {
      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
      JOptionPane.showMessageDialog(this,e.toString(),"������",JOptionPane.ERROR_MESSAGE);
    }
  }//GEN-LAST:event_jBtnM500ConnectedActionPerformed

  private void SendVCamPreset2MSV(int iVCam,int iPreset)
  {
    SerialPort comPort=openComPort();
    if(comPort!=null)
    {
      try
      {
        int iRd;
        byte[] rdBuf = new byte[7];
        String strSend="PM2_VCS+VCAMPRESET:"+String.format("%02d,%02d\r",iVCam,iPreset);
        comPort.writeBytes(strSend.getBytes(),strSend.length());
        iRd=comPort.readBytes(rdBuf, rdBuf.length);
        if((iRd==7) && (rdBuf[0]=='O') && (rdBuf[1]=='K'))
        {
          
        }
      }
      catch (Exception e)
      { e.printStackTrace(); }
      comPort.closePort();
    }
  }

  private void jTableMicVCSMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jTableMicVCSMouseClicked
  {//GEN-HEADEREND:event_jTableMicVCSMouseClicked
    if(evt.getButton()==MouseEvent.BUTTON3)
    {
      Point pt=evt.getPoint();
      int iColumn=jTableMicVCS.columnAtPoint(pt);
      int iRow=jTableMicVCS.rowAtPoint(pt);
      if((iRow>=0) && (iColumn==2))
      {
        int iVCam=GetIntCellValue(jTableMicVCS,iRow,1);
        int iPreset=GetIntCellValue(jTableMicVCS,iRow,2);
        if((iVCam>0) && (iPreset>0))
        {
          jTableMicVCS.changeSelection(iRow,iColumn,false,false);
          SendVCamPreset2MSV(iVCam,iPreset);
        }
      }
    }
  }//GEN-LAST:event_jTableMicVCSMouseClicked

  private void jTableVCamSNMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jTableVCamSNMouseClicked
  {//GEN-HEADEREND:event_jTableVCamSNMouseClicked
    if(evt.getButton()==MouseEvent.BUTTON3)
    {
      Point pt=evt.getPoint();
      int iColumn=jTableVCamSN.columnAtPoint(pt);
      int iRow=jTableVCamSN.rowAtPoint(pt);
      if((iRow>=0) && (iColumn==1))
      {
        int iVCam=GetIntCellValue(jTableVCamSN,iRow,0);
        String strSN=GetStringCellValue(jTableVCamSN,iRow,1);
        if((iVCam<=0) || strSN.isEmpty()) return;
        try
        {
          int iPort=Integer.parseInt(jTF_YealinkPort.getText());
          String strIP=jTF_YealinkIP.getText();
          jTableVCamSN.changeSelection(iRow,iColumn,false,false);
          M500TestCmdDlg dlg=new M500TestCmdDlg(this);
          dlg.startM500TestCmd(strSN,strIP,iPort);
        }
        catch(Exception e)
        { }
      }
    }

  }//GEN-LAST:event_jTableVCamSNMouseClicked

  SerialPort GetVCamComPort()
  {
    int i;
    SerialPort comPort=null;
    String strIF;

    strIF=(String)jCB_VCamIF.getSelectedItem();
    for(i=0;i<VCamIF.length;i++)
    {
      if(strIF.compareTo(VCamIF[i].getSystemPortName())==0)
      {
        int iSpeed;
        comPort=VCamIF[i];
        iSpeed=Integer.parseInt((String)jCB_VCamSpeed.getSelectedItem());
        comPort.openPort();
        comPort.setComPortParameters(iSpeed,8,SerialPort.ONE_STOP_BIT,SerialPort.NO_PARITY);
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING,100,100);
        break;
      }
    }
    return comPort;
  }

  void VCamZoom_SendCmd(int iZoom)
  {
    SerialPort comPort=GetVCamComPort();

    if(comPort==null) return;
    try
    {
      String strProto=(String)jCB_VCamProto.getSelectedItem();
      int iAddr=jCB_VCamAddr.getSelectedIndex()+1;
      byte[] btBuf = new byte[7];

      if(strProto.compareTo("Visca")==0)
      {
        btBuf[0]=(byte)(0x80 | iAddr);
        btBuf[1]=0x01; btBuf[2]=0x04; btBuf[3]=0x07;
        if(iZoom==0) btBuf[4]=0x00; // stop Zooming
        else
        {
          if(iZoom>0) btBuf[4]=0x20; // Zoom tele
          else btBuf[4]=0x30; // Zoom wide
        }
        btBuf[5]=(byte)0xff;
        comPort.writeBytes(btBuf,6);
      }
      else
      { // Pelco-D
        btBuf[0]=(byte)0xff;
        btBuf[1]=(byte)iAddr;
        btBuf[2]=0; btBuf[3]=0;
        if(iZoom>0) btBuf[3]|=0x40;
        if(iZoom<0) btBuf[3]|=0x20;
        btBuf[4]=0x00; btBuf[5]=0x00;
        iAddr=iAddr+btBuf[3];
        btBuf[6]=(byte)(iAddr & 0xff);
        comPort.writeBytes(btBuf,7);
      }
    }
    catch(Exception e)
    { e.printStackTrace(); }
  }

  void VCamPTDrive_SendCmd(int iPan,int iTilt)
  {
    SerialPort comPort=GetVCamComPort();

    if(comPort==null) return;
    try
    {
      String strProto=(String)jCB_VCamProto.getSelectedItem();
      int iAddr=jCB_VCamAddr.getSelectedIndex()+1;
      byte[] btBuf = new byte[15];

      if(strProto.compareTo("Visca")==0)
      {
        btBuf[0]=(byte)(0x80 | iAddr);
        btBuf[1]=0x01; btBuf[2]=0x06; btBuf[3]=0x03;
        btBuf[4]=0x0c; // Pan speed - midle
        btBuf[5]=0x0a; // Tilt speed - midle
        if(iPan==0)
        { btBuf[6]=0x00; btBuf[7]=0x00; btBuf[8]=0x00; btBuf[9]=0x00; }
        else
        {
          if(iPan>0)
          { btBuf[6]=0x00; btBuf[7]=0x00; btBuf[8]=0x01; btBuf[9]=0x04; }
          else
          { btBuf[6]=0x0f; btBuf[7]=0x0f; btBuf[8]=0x0f; btBuf[9]=0x02; }
        }
        if(iTilt==0)
        { btBuf[10]=0x00; btBuf[11]=0x00; btBuf[12]=0x00; btBuf[13]=0x00; }
        else
        {
          if(iTilt>0)
          { btBuf[10]=0x00; btBuf[11]=0x00; btBuf[12]=0x01; btBuf[13]=0x02; }
          else
          { btBuf[10]=0x0f; btBuf[11]=0x0f; btBuf[12]=0x0f; btBuf[13]=0x04; }
        }
        btBuf[14]=(byte)0xff;
        comPort.writeBytes(btBuf,btBuf.length);
      }
      else
      { // Pelco-D
        btBuf[0]=(byte)0xff;
        btBuf[1]=(byte)iAddr;
        btBuf[2]=0; btBuf[3]=0;
        if(iPan>0) btBuf[3]|=0x02;
        if(iPan<0) btBuf[3]|=0x04;
        if(iPan!=0) btBuf[4]=0x20;
        else btBuf[4]=0x00;
        if(iTilt>0) btBuf[3]|=0x08;
        if(iTilt<0) btBuf[3]|=0x10;
        if(iTilt!=0) btBuf[5]=0x20;
        else btBuf[5]=0x00;
        iAddr=iAddr+btBuf[3]+btBuf[4]+btBuf[5];
        btBuf[6]=(byte)(iAddr & 0xff);
        comPort.writeBytes(btBuf,7);
      }
    }
    catch(Exception e)
    { e.printStackTrace(); }
  }

  void VCamMem_SendCmd(int iCmd)
  {
    boolean bOk=false;
    SerialPort comPort=GetVCamComPort();

    if(comPort==null) return;
    try
    {
      int iV;
      String strProto=(String)jCB_VCamProto.getSelectedItem();
      int iPreset=Integer.parseInt(jTF_VCamPreset.getText());
      int iAddr=jCB_VCamAddr.getSelectedIndex()+1;
      byte[] btBuf = new byte[7];

      if(strProto.compareTo("Visca")==0)
      {
        btBuf[0]=(byte)(0x80 | iAddr);
        btBuf[1]=0x01; btBuf[2]=0x04; btBuf[3]=0x3f;
        btBuf[4]=(byte)iCmd;
        btBuf[5]=(byte)(iPreset & 0xff);
        btBuf[6]=(byte)0xff;
        comPort.writeBytes(btBuf,btBuf.length);
        iV=comPort.readBytes(btBuf,3);
        if((iV==3) && (btBuf[0]==(byte)((iAddr+8)<<4)) &&
           (btBuf[2]==(byte)0xff))
        {
          if((btBuf[1]==0x41) || (btBuf[1]==0x51))
          { bOk=true; }
        }
      }
      else
      { // Pelco-D
        int [] Visca2PelcoD={5,3,7};
        if(iPreset<1) iPreset=1;
        if(iPreset>20) iPreset=20;
        btBuf[0]=(byte)0xff;
        btBuf[1]=(byte)iAddr;
        btBuf[2]=0x00;
        btBuf[3]=(byte)Visca2PelcoD[iCmd];
        btBuf[4]=0x00;
        btBuf[5]=(byte)iPreset;
        btBuf[6]=(byte)(btBuf[1]+btBuf[3]+btBuf[5]);
        comPort.writeBytes(btBuf,btBuf.length);
        iV=comPort.readBytes(btBuf,4);
        if((iV==4) &&
           (btBuf[0]==0xff) &&
           (btBuf[1]==(byte)iAddr) &&
           (btBuf[3]==(byte)(btBuf[1]+btBuf[2])) &&
           (btBuf[2]==0))
        { bOk=true; }
      }
    }
    catch (Exception e)
    { e.printStackTrace(); }
    comPort.closePort();
    if(bOk)
    {
      String msg="<html><p align=\"center\">������� ��������� �������!</p>";
      JOptionPane.showMessageDialog(this,msg,"��������",JOptionPane.WARNING_MESSAGE);
    }
    else
    {
      String msg="<html><p align=\"center\">������� �� ���������!</p>";
      JOptionPane.showMessageDialog(this,msg,"������",JOptionPane.ERROR_MESSAGE);
    }
    return;
  }

  void StopGrabber()
  {
    if(grabber!=null)
    {
      VCamTimer.stop();
      grabber.stop();
      grabber=null;
      if(cFrame!=null)
      { cFrame.dispose(); cFrame=null; }
      Graphics vcg= VCamCanvas.getGraphics();
      vcg.clearRect(0, 0, VCamCanvas.getWidth(),VCamCanvas.getHeight());
      String strInfo="WAIT VIDEO...";
      int iStrW=vcg.getFontMetrics().stringWidth(strInfo);
      vcg.drawString(strInfo,
                     (VCamCanvas.getWidth()-iStrW)/2, VCamCanvas.getHeight()/2);
      vcg.dispose();
    }
  }

  void StartGrabber()
  {
    StopGrabber();
    if(m_iWebCamInd<0) return;
    do
    {
      grabber=new OpenCVBIGrabberTS(m_iWebCamInd);
      if(grabber==null) return;
      if(grabber.start(VCamCanvas.getWidth(),VCamCanvas.getHeight())) break;
      m_iWebCamInd--;
    }
    while(m_iWebCamInd>-1);
    if(m_iWebCamInd<0) StopGrabber();
    else
    {
      grabber.setRotate(jCheckBox_Rotate.isSelected());
      grabber.setMirror(jCheckBox_Mirror.isSelected());
      jCB_VCam.setSelectedIndex(m_iWebCamInd);
      VCamTimer.start();
    }
  }

  public static void main(String args[])
  {
    pm2vcscfg=new PM2_VCS_CFG();
    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable()
    {
      @Override
      public void run()
      {
        pm2vcscfg.setVisible(true);
      }
    });
  }

  static PM2_VCS_CFG pm2vcscfg;

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private java.awt.Canvas VCamCanvas;
  private javax.swing.JButton jBt_Down;
  private javax.swing.JButton jBt_Left;
  private javax.swing.JButton jBt_Rigth;
  private javax.swing.JButton jBt_Up;
  private javax.swing.JButton jBt_VCamSetting;
  private javax.swing.JButton jBt_ZoomM;
  private javax.swing.JButton jBt_ZoomP;
  private javax.swing.JButton jBtnConnect;
  private javax.swing.JButton jBtnM500Connected;
  private javax.swing.JButton jBtnReset;
  private javax.swing.JButton jBtnSendCmdMicStatus;
  private javax.swing.JButton jBtnTestTCP;
  private javax.swing.JButton jButtonCallPreset;
  private javax.swing.JButton jButtonDelPreset;
  private javax.swing.JButton jButtonExit;
  private javax.swing.JButton jButtonSavePreset;
  private javax.swing.JButton jButtonWrite;
  private javax.swing.JComboBox<String> jCB_CamDef;
  private javax.swing.JComboBox<String> jCB_IF;
  private javax.swing.JComboBox<String> jCB_PM2Speed;
  private javax.swing.JComboBox<String> jCB_Proto;
  private javax.swing.JComboBox<String> jCB_Speed;
  private javax.swing.JComboBox<String> jCB_VCam;
  private javax.swing.JComboBox<String> jCB_VCamAddr;
  private javax.swing.JComboBox<String> jCB_VCamIF;
  private javax.swing.JComboBox<String> jCB_VCamProto;
  private javax.swing.JComboBox<String> jCB_VCamSpeed;
  private javax.swing.JCheckBox jCheckBox_Mirror;
  private javax.swing.JCheckBox jCheckBox_Rotate;
  private javax.swing.JComboBox<String> jComboBoxRS232;
  private javax.swing.JDialog jDlgTestTCP;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel10;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel13;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel17;
  private javax.swing.JLabel jLabel18;
  private javax.swing.JLabel jLabel19;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel20;
  private javax.swing.JLabel jLabel21;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JLabel jLblGate;
  private javax.swing.JLabel jLblIp;
  private javax.swing.JLabel jLblMask;
  private javax.swing.JLabel jLblPort;
  private javax.swing.JLabel jLblUdpPort;
  private javax.swing.JLabel jLblYealinkIp;
  private javax.swing.JLabel jLblYealinkPort;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JScrollPane jScrollPane2;
  private javax.swing.JScrollPane jScrollPane3;
  private javax.swing.JScrollPane jScrollPane4;
  private javax.swing.JToolBar.Separator jSeparator1;
  private javax.swing.JToolBar.Separator jSeparator2;
  private javax.swing.JToolBar jTB_PTZ;
  private javax.swing.JTextField jTF_Gate;
  private javax.swing.JTextField jTF_IP;
  private javax.swing.JTextField jTF_Mask;
  private javax.swing.JTextField jTF_Port;
  private javax.swing.JTextField jTF_UdpPort;
  private javax.swing.JTextField jTF_VCamPreset;
  private javax.swing.JTextField jTF_YealinkIP;
  private javax.swing.JTextField jTF_YealinkPort;
  private javax.swing.JTabbedPane jTabbedPane;
  private javax.swing.JTable jTableMicVCS;
  private javax.swing.JTable jTableVCamSN;
  private javax.swing.JTextArea jTextAreaTCP;
  private javax.swing.JTextArea jTxtM500Result;
  // End of variables declaration//GEN-END:variables

}

/*
//IPCamGrabberTS grabber;
grabber=new IPCamGrabberTS("rtsp://192.168.1.2:8080/h264_ulaw.sdp");
//grabber=new IPCamGrabberTS("http://192.168.1.2:8080/video");
if(grabber!=null)
{
  grabber.start(VCamCanvas.getWidth(),VCamCanvas.getHeight());
  VCamTimer.start();
}*/
