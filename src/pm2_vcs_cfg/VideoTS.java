package pm2_vcs_cfg;

import by.tsoft.v4l2java;
import org.bytedeco.videoinput.videoInput;

public class VideoTS
{
  static int iOS; // 0 - windows, 1 - linux(unix), 2- macos
  videoInput VI;
  v4l2java V4L2J;

  public VideoTS()
  {
    if(iOS==0) VI=new videoInput();
    else V4L2J=new v4l2java();
  }

  public static int listDevices()
  {
    if(iOS==0) return videoInput.listDevices();
    else return v4l2java.listDevices();
  }

  public static String getDeviceName(int iDev)
  {
    if(iOS==0) return videoInput.getDeviceName(iDev).getString();
    else return v4l2java.getDeviceName(iDev);
  }

  public void showSettingsWindow(int deviceNumber)
  {
    if(iOS==0) VI.showSettingsWindow(deviceNumber);
// !!! �� ����������� ��� LINUX!!! }
  }

  public void setupDevice(int deviceNumber)
  {
    if(iOS==0) VI.setupDevice(deviceNumber);
    else V4L2J.setupDevice(deviceNumber);
  }

  public int getWidth(int deviceNumber)
  {
    if(iOS==0) return VI.getWidth(deviceNumber);
    else return V4L2J.getWidth(deviceNumber);
  }

  public int getHeight(int deviceNumber)
  {
    if(iOS==0) return VI.getHeight(deviceNumber);
    else return V4L2J.getHeight(deviceNumber);
  }

  public int getSize(int deviceNumber)
  {
    if(iOS==0) return VI.getSize(deviceNumber);
    else return V4L2J.getSize(deviceNumber);
  }

  public void stopDevice(int deviceNumber)
  {
    if(iOS==0) VI.stopDevice(deviceNumber);
    else V4L2J.stopDevice(deviceNumber);
  }

  public void close()
  {
    if(iOS==0) VI.close();
    else V4L2J.close();
  }

  public boolean isFrameNew(int deviceNumber)
  {
    if(iOS==0) return VI.isFrameNew(deviceNumber);
    else return V4L2J.isFrameNew(deviceNumber);
  }

  public boolean getPixels(int deviceNumber,byte[] targetPixels,boolean bln,boolean bNoRotate)
  {
    if(iOS==0) return VI.getPixels(deviceNumber,targetPixels,bln,bNoRotate);
    else return V4L2J.getPixels(deviceNumber,targetPixels,bln,bNoRotate);
  }

    static
    {
      String os = System.getProperty("os.name").toLowerCase();
      iOS=0; //windows
      if(os.contains("mac")) iOS=2; // macos
      else
      {
        if((os.contains("nux")) || (os.contains("nix"))) iOS=1; // unix || linux
      }
    }
}
