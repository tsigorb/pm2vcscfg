package pm2_vcs_cfg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

public class SocketThread extends Thread
{
  private boolean bRun,bExit;
  private Socket m_sTcp;
  private int m_iPort;

  LinkedBlockingQueue<XchgPckt> m_bqIn;
  LinkedBlockingQueue<XchgPckt> m_bqOut;

  LinkedList<XchgWithClientThrd> clientList;

  class XchgWithClientThrd extends Thread
  {
    private boolean m_bExit,m_bRun;
    private Socket sock; // �����, ����� ������� ������ �������� � ��������,
    private LinkedBlockingQueue<XchgPckt> m_bqIn;

    public XchgWithClientThrd(Socket sock,LinkedBlockingQueue<XchgPckt> bqIn)
    {
      m_bExit=false;
      this.sock = sock;
      m_bqIn=bqIn;
      start(); // �������� run()
    }

    public void Exited()
    { m_bRun=false; }

    public boolean isEnd()
    { return m_bExit; }

    @Override
    public void run()
    {
      String strData;
      BufferedReader br=null;
      XchgPckt pckt=new XchgPckt();

      pckt.strData="MSV is connected to Yealink MeetingEye...\r\n";
      pckt.iOp=4;
      pckt.iData=0;
      m_bqIn.add(pckt);
      try
      {
        sock.setSoTimeout(50);
        InputStream is = sock.getInputStream();
        br=new BufferedReader(new InputStreamReader(is));
      }
      catch (Exception e) { }
      if(br!=null)
      {
        m_bRun=true;
        while(m_bRun)
        {
          do
          {
            strData=null;
            try
            { strData=br.readLine(); }
            catch(Exception e)
            { }
            if(strData==null) break;
            pckt=new XchgPckt();
            pckt.strData="MSV -> Yealink MeetingEye: "+strData+"\r\n";
            pckt.iOp=4;
            pckt.iData=0;
            m_bqIn.add(pckt);
          }
          while(true);
          try { Thread.sleep(20); }
          catch(Exception ex) { }
        }
      }
      try { sock.close(); }
      catch(Exception ex) { }
      pckt=new XchgPckt();
      pckt.strData="MSV disconnect from Yealink MeetingEye!\r\n";
      pckt.iOp=4;
      pckt.iData=0;
      m_bqIn.add(pckt);
      m_bExit=true;
    }
  }

  SocketThread(String name,LinkedBlockingQueue<XchgPckt> bqOut,LinkedBlockingQueue<XchgPckt> bqIn,int iPort)
  {
    super(name);
    m_bqIn=bqIn; m_bqOut=bqOut;
    m_iPort=iPort;
    m_sTcp=null;
    bExit=true;
  }

  void Exited()
  {
    bRun=false;
    while(!bExit)
    {
      try { Thread.sleep(100); }
      catch(Exception ex)
      { System.out.println("SocketThread.Exited: "+ex.toString()); }
    }
  }

  void sendIpPacket(XchgPckt pckt)
  {
    switch(pckt.iOp)
    {
      case 1: // connect
        try
        {
          m_sTcp = new Socket (pckt.strData,pckt.iData);
          m_sTcp.setSoTimeout(20);
          pckt.iData=0;
        }
        catch (Exception e) 
        { pckt.iData=-1; pckt.strData=e.toString()+"\r\n"; }
        pckt.iOp=11;
        m_bqIn.add(pckt);
        break;
      case 2: // disconnect
        try
        {
          m_sTcp.close();
          pckt.iData=0;
        }
        catch (Exception e) 
        { pckt.iData=-1; pckt.strData=e.toString()+"\r\n"; }
        m_sTcp=null;
        pckt.iOp=12;
        m_bqIn.add(pckt);
        break;
      case 3: // write
        try
        {
          m_sTcp.getOutputStream().write(pckt.strData.getBytes());
          pckt.iData=0;
        }
        catch (Exception e) 
        { pckt.iData=-1; pckt.strData=e.toString()+"\r\n"; }
        pckt.iOp=13;
        m_bqIn.add(pckt);
        break;
      default:
        pckt.iData=-1;
        pckt.strData="Unknown socket command: "+pckt.iOp.toString()+"\r\n";
        pckt.iOp=10;
        m_bqIn.add(pckt);
        break;
    }
  }

  @Override
  public void run()
  {
    int i;
    ServerSocket server=null;
    BufferedReader br=null;

    clientList = new LinkedList<>();

    if(m_iPort!=0)
    {
      try
      {
        server = new ServerSocket(m_iPort,1);
        server.setSoTimeout(50);
      }
      catch (IOException e) {}
    }
    bRun=true;
    bExit=false;
    while(bRun)
    {
      if(m_sTcp!=null)
      {
        if(br==null)
        {
          try
          { br=new BufferedReader(new InputStreamReader(m_sTcp.getInputStream())); }
          catch (Exception e) { }
        }
        else
        {
          do
          {
            try
            {
              XchgPckt pckt=new XchgPckt();
              pckt.strData=br.readLine();
              if(pckt.strData==null) break;
              pckt.strData+="\r\n";
              pckt.iOp=4;
              pckt.iData=0;
              m_bqIn.add(pckt);
            }
            catch(IOException e) { break; }
          }
          while(true);
        }
      }
      else br=null;
      if(server!=null)
      {
        for(i=0;i<clientList.size();i++)
        {
          if(clientList.get(i).isEnd()) clientList.remove(i);
        }
        try
        {
          Socket socket = server.accept(); // ����������� �� ������������� ������ ���������� ��� �-����
          while(clientList.size()!=0)
          { // ������� ��� ���������� ����������
            clientList.get(0).Exited();
            clientList.remove(0);
          }
          clientList.add(new XchgWithClientThrd(socket,m_bqIn)); // �������� ����� ����������� � ������
        }
        catch (IOException e)
        { }
      }
      else
      {
        try { Thread.sleep(20); }
        catch(Exception ex) { }
      }
      if(!m_bqOut.isEmpty()) sendIpPacket(m_bqOut.poll());
    }
    for(i=0;i<clientList.size();i++) clientList.get(i).Exited();
    while(!clientList.isEmpty())
    {
      if(clientList.get(0).isEnd()) clientList.remove(0);
      else
      {
        try { Thread.sleep(20); }
        catch(Exception ex) { }
      }
    }
    if(server!=null)
    {
      try
      { server.close(); }
      catch(IOException e) {}
    }
    bExit=true;
  }
}
